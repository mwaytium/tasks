﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab7_epam
{
    [Serializable]
    public class MatrixOutOfRangeException : Exception
    {
        private string _message = null;

        public string AddInf
        {
            get
            {
                return (_message == null) ? "отсутствует" : _message;
            }
        }

        public MatrixOutOfRangeException(string message) : base(message) {}
        public MatrixOutOfRangeException(string message, int rows1, int cols1, int rows2, int cols2) : base(message) {
            _message = "!\nкол-во строк:  \n" + "1 - " + rows1 + "\n2 - " + rows2 +
                "\nкол-во столбцов: \n" + "1 - " + cols1 + "\n2 - " + cols2;
        }
    }

    public class Matrix
    {
        Random rand = new Random();
        private double[,] matrix;
        public int rows;
        public int cols;

        public double this[int i, int j]
        {
            set
            {
                matrix[i, j] = value;
            }

            get
            {
                return matrix[i, j];
            }
        }

        public Matrix(int rows, int cols)
        {
            this.rows = rows;
            this.cols = cols;
            matrix = new double[rows, cols];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    matrix[i, j] = rand.Next(10);
                }
            }
        }

        public Matrix GetEmpty(int rows, int cols)
        {
            Matrix newMatrix = new Matrix(rows, cols);

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    newMatrix[i, j] = 0;
                }
            }
            return newMatrix;
        }

        public void sum(Matrix matrix)
        {
            if (this.rows != matrix.rows | this.cols != matrix.cols)
            {
                throw new MatrixOutOfRangeException("разные размеры", rows, cols, matrix.rows, matrix.cols);
            }

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    this.matrix[i, j] += matrix[i, j];
                }
            }
        }

        public void diff(Matrix matrix)
        {
            if (this.rows != matrix.rows | this.cols != matrix.cols)
            {
                throw new MatrixOutOfRangeException("разные размеры", rows, cols, matrix.rows, matrix.cols);
            }

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    this.matrix[i, j] -= matrix[i, j];
                }
            }
        }

        public static Matrix mult(Matrix matrix, Matrix matrix2)
        {
            if (matrix.cols != matrix2.rows)
            {
                throw new MatrixOutOfRangeException("Не совпадают размерности матриц");
            }
                
            Matrix temp = new Matrix(matrix.rows, matrix2.cols);
            for (int i = 0; i < matrix.rows; ++i)
            {
                for (int j = 0; j < matrix2.cols; ++j)
                {
                    temp[i, j] = 0;
                    for (int k = 0; k < matrix.cols; ++k)
                    { 
                        temp[i, j] += matrix[i, k] * matrix2[k, j];
                    }
                }
            }
            return temp;
        }

        public string printMatrix()
        {
            StringBuilder str = new StringBuilder();

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    str.Append(matrix[i, j] + " ");
                }
                str.AppendLine();
            }
            return str.ToString();
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            Matrix matrix = new Matrix(2, 3);
            Matrix matrix2 = new Matrix(6, 2);
            Matrix matrix3;

            try
            {
                matrix.sum(matrix2);
                matrix3 = Matrix.mult(matrix, matrix2);
                Console.WriteLine("Матрица 1");
                Console.WriteLine(matrix.printMatrix());
                Console.WriteLine("Матрица 2");
                Console.WriteLine(matrix2.printMatrix());
                Console.WriteLine("Произведение");
                Console.WriteLine(matrix3.printMatrix());
            }
            catch (MatrixOutOfRangeException e)
            {
                e = new MatrixOutOfRangeException(e.Message + " доп.инф " + e.AddInf);
                Console.WriteLine(e);
            }
            
        }
    }
}
