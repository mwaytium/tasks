﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using lab7_epam;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab7_epam.Tests
{
    [TestClass()]
    public class MatrixTests
    {
        Matrix matrix1 = new Matrix(3, 3);
        Matrix matrix2 = new Matrix(3, 3);
        
        [TestMethod()]
        public void sumTest()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    matrix1[i, j] = 1;
                    matrix2[i, j] = 1;
                }
            }

            matrix1.sum(matrix2);
            Matrix actual = matrix1;

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    matrix2[i, j] = 2;
                }
            }

            Matrix expected = matrix2;

            //assert
            Assert.AreEqual(expected.printMatrix(), actual.printMatrix());
        }

        [TestMethod()]
        public void diffTest()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    matrix1[i, j] = 1;
                    matrix2[i, j] = 1;
                }
            }

            matrix1.diff(matrix2);
            Matrix actual = matrix1;

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    matrix2[i, j] = 0;
                }
            }

            Matrix expected = matrix2;

            //assert
            Assert.AreEqual(expected.printMatrix(), actual.printMatrix());
        }

        [TestMethod()]
        public void multTest()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    matrix1[i, j] = 1;
                    matrix2[i, j] = 1;
                }
            }

            Matrix actual = Matrix.mult(matrix1, matrix2);

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    matrix2[i, j] = 3;
                }
            }

            Matrix expected = matrix2;

            //assert
            Assert.AreEqual(expected.printMatrix(), actual.printMatrix());
        }
    }
}