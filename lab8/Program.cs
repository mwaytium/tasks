﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab8
{
    class Program
    {
        static void Main(string[] args)
        {
            TextReader text = null;
            try
            {
                char[] c = { '[', '0', '1', ']', ' ', 'П', 'р', 'и', 'в', 'е', 'т', ' ', 'м', 'и', 'р', '!' };
                text = TextReader.Create("text.txt", 10);
                text.writeChar(3, 't');
                text.writeChar(4, 'e');
                text.writeText(c);
                Console.WriteLine(text.ReadAllFile());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                text.Dispose();
            }

            using (text = TextReader.Read("text.txt"))
            {
                text.writeChar(2, '2');
                Console.WriteLine(text.ReadAllFile());
            }
                
            
        }
    }

    class TextReader : IDisposable
    {
        private string filePath;
        public int length;
        private FileStream openFile;
        private FileStream createFile;
        private StreamWriter writer;
        private StreamReader reader;
        private char[] buffer;

        private TextReader(string filePath, int length)
        {
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }

            this.filePath = filePath;
            this.length = length;
            buffer = new char[length];
            openStream(true, true, false);

            for (int i = 0; i < length; i++)
            {
                buffer[i] = ' ';
                writer.Write(" ");
            }
            Close(true, true, false);
        }

        public static TextReader Create(string filePath, int length)
        {
            return new TextReader(filePath, length);
        }

        private TextReader(string filePath)
        {
            this.filePath = filePath;
            openStream(true, false, true);
            buffer = reader.ReadToEnd().ToCharArray();
            Close(true, false, true);
        }

        public static TextReader Read(string filePath)
        {
            return new TextReader(filePath);
        }

        private void openStream(bool bfile, bool bwrite, bool breader)
        {
            if (bfile) createFile = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            if (bwrite) writer = new StreamWriter(createFile, Encoding.Default);
            if (breader) reader = new StreamReader(createFile, Encoding.Default);
        }

        private void Close(bool bfile, bool bwrite, bool breader)
        {
            if (bfile) createFile.Flush();
            if (bwrite) writer.Close();
            if (breader) reader.Close();
        }

        public string ReadAllFile()
        {
            try
            {
                openStream(true, false, true);
                return reader.ReadToEnd();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                Close(true, false, true);
            }

        }

        public void writeChar(int index, char c)
        {
            try
            {
                openStream(true, true, false);

                for (int i = 0; i < buffer.Length; i++)
                {
                    if (i == index) buffer[i] = c;
                    writer.Write(buffer[i]);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                Close(true, true, false);
            }
        }

        public void writeText(char[] c)
        {
            try
            {
                openStream(true, true, false);
                buffer = c;
                length = c.Length;

                for (int i = 0; i < length; i++)
                {
                    writer.Write(buffer[i]);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                Close(true, true, false);
            }
        }

        public char readText(int index)
        {
            return buffer[index];
        }

        public void Dispose()
        {
            createFile.Close();
        }
    }
}
