﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    class Client
    {
        private IDirector director;
        private IEmployee employee;

        public Client(IFactory factory)
        {
            director = factory.CreateDirector();
            employee = factory.CreateEmployee();
        }

        public void Run()
        {
            director.Do();
            employee.Do();
        }
    }
}
