﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    class FactoryDirector : IDirector
    {
        public void Do()
        {
            Console.WriteLine("FactoryDirector \n");
        }
    }
}
