﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    class FactoryEmployee : IEmployee
    {
        public void Do()
        {
            Console.WriteLine("FactoryEmployee \n");
        }
    }
}
