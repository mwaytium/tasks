﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    class StallFactory : IFactory
    {
        public IDirector CreateDirector()
        {
            return new StallDirector();
        }
        public IEmployee CreateEmployee()
        {
            return new StallEmployee();
        }
    }
}
