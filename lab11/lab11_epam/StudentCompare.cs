﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab11_epam
{
    class StudentCompare : IComparer<Exam>
    {
        public int Compare(Exam left, Exam right)
        {
            if (ReferenceEquals(left, null) || ReferenceEquals(right, null))
                throw new ArgumentNullException();
            else if (String.Compare(left.Name, right.Name) == 1)
                return 1;
            else if (String.Compare(left.Name, right.Name) == 0)
                return 0;
            else
                return -1;
        }
    }
}
