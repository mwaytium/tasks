﻿using System;
using System.Collections.Generic;

namespace lab11_epam
{
    public class Exam : IEquatable<Exam>, IComparable<Exam>, IComparer<Exam>
    {
        private string name;
        private string test;
        private int date;
        private int grade;

        public string Name
        {
            get { return name; }
            private set
            {
                if (ReferenceEquals(value, null))
                    throw new ArgumentNullException();
                name = value;
            }
        }

        public string Test
        {
            get { return test; }
            private set
            {
                if (ReferenceEquals(value, null))
                    throw new ArgumentNullException();
                test = value;
            }
        }

        public int Grade
        {
            get { return grade; }
            private set
            {
                if (value < 0)
                    throw new ArgumentException();
                grade = value;
            }
        }

        public int Date
        {
            get { return date; }
            private set
            {
                date = value;
            }

        }

        public Exam(string name, string test, int grade, int date)
        {
            Name = name;
            Test = test;
            Grade = grade;
            Date = date;
        }

        public bool Equals(Exam otherTest)
        {
            if (ReferenceEquals(otherTest, null)) return false;
            if (ReferenceEquals(otherTest, this)) return true;
            return EqulsProperties(ref otherTest);
        }

        private bool EqulsProperties(ref Exam otherTest)
        {
            if (otherTest.Name != Name)
                return false;
            if (otherTest.Test == Test)
                return false;
            if (otherTest.Grade == Grade)
                return false;
            if (otherTest.Date == Date)
                return false;
            return true;
        }

        public int Compare(Exam test1, Exam test2)
        {
            if (ReferenceEquals(test1, null) || ReferenceEquals(test2, null))
                throw new ArgumentNullException();
            return test1.CompareTo(test2);
        }

        public int CompareTo(Exam otherTest)
        {
            if (ReferenceEquals(test, null))
                throw new ArgumentNullException();
            if (otherTest.Grade < Grade) return 1;
            if (otherTest.Grade > Grade) return -1;
            return 0;
        }

        public override string ToString()
        {
            return Name + " " + Test + " " + Date + " " + Grade;
        }
    }
}
