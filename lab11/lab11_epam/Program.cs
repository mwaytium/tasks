﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab11_epam
{
    class Program
    {
        static void Main(string[] args)
        {
            var binaryTree1 =
               new BinarySearchTree<Exam>(new Exam[]
               {
                    new Exam("Vasya", "title", 5, 250606),
                    new Exam("Pete", "title", 3, 250606),
                    new Exam("Petr", "title", 2, 250606),
                    new Exam("Fedya", "title", 4, 250606),
              }, new StudentCompare());

            foreach (Exam b in binaryTree1.Inorder())
                Console.Write(b + "\n");


           // var binaryTree1 = new BinarySearchTree<Int32>(new Int32[]
           //     {
           //     5,3,2,4,6,7
           //     });

           // foreach (Int32 b in binaryTree1.Postorder()) {
           //     Console.Write(b + "\n");
           // }

        }
    }
}
