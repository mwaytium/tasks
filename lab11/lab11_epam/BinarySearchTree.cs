﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace lab11_epam
{
    public sealed class BinarySearchTree<T> : IEnumerable<T>
    {
        private Node head;
        private readonly IComparer<T> comparer;

        public BinarySearchTree()
        {
            head = null;
        }

        public BinarySearchTree(IEnumerable<T> collection, IComparer<T> comparer)
        {
            if (ReferenceEquals(collection, null))
            {
                throw new ArgumentNullException(nameof(collection));
            }
            if (ReferenceEquals(comparer, null))
            {
                this.comparer = Comparer<T>.Default;
            }
            else
            {
                this.comparer = comparer;
            }

            foreach (var item in collection)
            {
                Add(item);
            }
        }

        public BinarySearchTree(IEnumerable<T> collection) : this(collection, null) { }

        public void Add(T value)
        {
            if (ReferenceEquals(value, null))
            {
                throw new ArgumentNullException(nameof(value));
            }
            if (ReferenceEquals(head, null))
            {
                head = new Node(value);
                return;
            }

            AddTo(head, value);
        }

        private void AddTo(Node node, T value)
        {
            if (comparer.Compare(node.Value, value) > 0)
            {
                if (node.Left == null)
                {
                    node.Left = new Node(value);
                }
                else
                {
                    AddTo(node.Left, value);
                }
            }
            else
            {
                if (node.Right == null)
                {
                    node.Right = new Node(value);
                }
                else
                {
                    AddTo(node.Right, value);
                }
            }
        }

        public void Clear()
        {
            head = null;

        }

        public IEnumerator<T> GetEnumerator()
        {
            return Inorder().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerable<T> Preorder() => Preorder(head);

        public IEnumerable<T> Postorder() => Postorder(head);

        public IEnumerable<T> Inorder() => Inorder(head);

        private IEnumerable<T> Preorder(Node node)
        {
            if (node == null)
                yield break;

            yield return node.Value;

            foreach (var e in Preorder(node.Left))
                yield return e;

            foreach (var e in Preorder(node.Right))
                yield return e;
        }

        private IEnumerable<T> Inorder(Node node)
        {
            if (node == null)
                yield break;

            foreach (var e in Inorder(node.Left))
                yield return e;

            yield return node.Value;

            foreach (var e in Inorder(node.Right))
                yield return e;
        }

        private IEnumerable<T> Postorder(Node node)
        {
            if (node == null)
                yield break;

            foreach (var e in Postorder(node.Left))
                yield return e;

            foreach (var e in Postorder(node.Right))
                yield return e;

            yield return node.Value;
        }

        public class Node
        {
            private T value;
            public Node Left { get; set; }
            public Node Right { get; set; }
            public T Value
            {
                get
                {
                    return value;
                }
                set
                {
                    if (ReferenceEquals(value, null))
                        throw new ArgumentException("Значение не должно быть null");

                    this.value = value;
                }
            }

            public Node(T value)
            {
                Value = value;
            }
        }
    }
}