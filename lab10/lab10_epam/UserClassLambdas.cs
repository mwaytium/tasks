﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace lab10_epam
{
    class UserClassLambdas : ICutDownNotifier
    {
        private StartTaskTime startTaskTime = null;
        private Action<string, int> finishTaskTime = null;
        private Form1 form;
        private Timer timer;
        
        public UserClassLambdas(Form1 form, StartTaskTime startTaskTime, Action<string, int> finishTaskTime)
        {
            this.form = form;
            this.startTaskTime = startTaskTime;
            this.finishTaskTime = finishTaskTime;
        }

        void ICutDownNotifier.Init(int count)
        {
            timer = new Timer();
            timer.start += new StartHandler(() => {
                startTaskTime.Invoke("«Выполнение задания»", count);
                form.label2.Text = "Старт обратного отсчёта";
                Thread.Sleep(1000);
            });
            timer.countDown += new TimerHandler(() => {
                while (count != 0)
                {
                    form.label2.Text = "Осталось " + count.ToString() + "-cекунд";
                    Thread.Sleep(1000);
                    count--;
                }
            });
            timer.finish += new FinishHandler(() => {
                finishTaskTime("«Выполнение задания»", count);
                form.label2.Text = "Обратный отсчёт завершён";
            });
        }

        void ICutDownNotifier.Run()
        {
            timer.TimerStart();
        }
    }
}
