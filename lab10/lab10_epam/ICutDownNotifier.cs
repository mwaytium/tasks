﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab10_epam
{
    interface ICutDownNotifier
    {
        void Init(int count);
        void Run();
    }
}
