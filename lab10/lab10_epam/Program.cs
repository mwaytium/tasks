﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace lab10_epam
{

    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }

    public delegate void NewThread();
    public delegate void TimerHandler();
    public delegate void StartHandler();
    public delegate void FinishHandler();

    class Timer
    {
        private NewThread thread = null;
        public TimerHandler countDown = null;
        public StartHandler start = null;
        public FinishHandler finish = null;

        public void TimerStart()
        {
            // новый тред чтоб синхронный Invoke() не тормозил основной тред
            thread = new NewThread(Run);
            thread.BeginInvoke(null, null);
        }

        private void Run()
        {
            start.Invoke();
            countDown.Invoke();
            finish.Invoke();
        }
    }
}
