﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace lab10_epam
{
    class UserClassMethods : ICutDownNotifier
    {
        private StartTaskTime startTaskTime = null;
        private Action<string, int> finishTaskTime = null;
        Form1 form;
        Timer timer;
        int count;

        public UserClassMethods(Form1 form, StartTaskTime startTaskTime, Action<string, int> finishTaskTime)
        {
            this.form = form;
            this.startTaskTime = startTaskTime;
            this.finishTaskTime = finishTaskTime;
        }

        void ICutDownNotifier.Init(int count)
        {
            timer = new Timer();
            this.count = count;
            timer.start += new StartHandler(Start);
            timer.countDown += new TimerHandler(CountDown);
            timer.finish += new FinishHandler(Finish);
        }

        void ICutDownNotifier.Run()
        {
            timer.TimerStart();
        }

        private void Start()
        {
            startTaskTime.Invoke("«Проверку задания перед отправкой»", count);
            form.label3.Text = "Старт обратного отсчёта";
            Thread.Sleep(1000);
        }

        private void CountDown()
        {
            while (count != 0)
            {
                form.label3.Text = "Осталось " + count.ToString() + "-cекунд";
                Thread.Sleep(1000);
                count--;
            }
        }

        private void Finish()
        {
            finishTaskTime("«Проверка задания перед отправкой»", count);
            form.label3.Text = "Обратный отсчёт завершён";
        }

    }
}
