﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab10_epam
{
    public delegate void StartTaskTime(string taskName, int amountOfTime);

    public partial class Form1 : Form
    {
        private Timer timer = null;
        private List<ICutDownNotifier> list = new List<ICutDownNotifier>();
        private ICutDownNotifier type1;
        private ICutDownNotifier type2;
        private ICutDownNotifier type3;

        public Form1()
        {
            InitializeComponent();
            timer = new Timer();

            type1 = new UserClassDelegates(this, new StartTaskTime((string taskName, int amountOfTime) =>
                label4.Text = "На " + taskName + " отведено " + amountOfTime + " сек."), 
                new Action<string, int>((string taskName, int amountOfTime) =>
                label5.Text = taskName + " завершено за " + amountOfTime + " сек."
                ));

            type2 = new UserClassLambdas(this, new StartTaskTime((string taskName, int amountOfTime) => 
                label6.Text = "На " + taskName + " отведено " + amountOfTime + " сек."),
                new Action<string, int>((string taskName, int amountOfTime) =>
                label7.Text = taskName + " завершено за " + amountOfTime + " сек."
                ));

            type3 = new UserClassMethods(this, new StartTaskTime((string taskName, int amountOfTime) =>
                label8.Text = "На " + taskName + " отведено " + amountOfTime + " сек."),
                new Action<string, int>((string taskName, int amountOfTime) =>
                label9.Text = taskName + " завершено за " + amountOfTime + " сек."
                ));

            list.Add(type1);
            list.Add(type2);
            list.Add(type3);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            type1.Init(Convert.ToInt32(textBox1.Text));
            type1.Run();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            type2.Init(Convert.ToInt32(textBox2.Text));
            type2.Run();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            type3.Init(Convert.ToInt32(textBox3.Text));
            type3.Run();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            list[0].Init(Convert.ToInt32(textBox1.Text));
            list[1].Init(Convert.ToInt32(textBox2.Text));
            list[2].Init(Convert.ToInt32(textBox3.Text));

            list.ForEach(type =>
            {
                type.Run();
            });
        }
    }
}