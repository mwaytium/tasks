﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace lab10_epam
{
    class UserClassDelegates : ICutDownNotifier
    {
        private StartTaskTime startTaskTime = null;
        private Action<string, int> finishTaskTime = null;
        Form1 form;
        Timer timer;

        public UserClassDelegates(Form1 form, StartTaskTime startTaskTime, Action<string, int> finishTaskTime)
        {
            this.form = form;
            this.startTaskTime = startTaskTime;
            this.finishTaskTime = finishTaskTime;
        }

        void ICutDownNotifier.Init(int count)
        {
            timer = new Timer();
            timer.start += new StartHandler(delegate ()
            {
                startTaskTime.Invoke("«Чтение задания»", count);
                form.label1.Text = "Старт обратного отсчёта";
                Thread.Sleep(1000);
            });
            timer.countDown += new TimerHandler(delegate ()
            {
                while (count != 0)
                {
                    form.label1.Text = "Осталось " + count.ToString() + "-cекунд";
                    Thread.Sleep(1000);
                    count--;
                }
            });
            timer.finish += new FinishHandler(delegate ()
            {
                finishTaskTime("«Чтение задания»", count);
                form.label1.Text = "Обратный отсчёт завершён";
            });
        }

        void ICutDownNotifier.Run()
        {
            timer.TimerStart();
        }
    }
}
